import Router from './Router';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';

// B.1. Sélectionner des éléments
// console.log(document.querySelector('.logo img'));
// console.log(document.querySelector('.pizzaFormLink'));
// console.log(document.querySelector('.pizzaThumbnail h4'));

// console.log(document.querySelectorAll('.mainMenu a'));
// console.log(document.querySelectorAll('.pizzaThumbnail li'));

// B.2. Modifier des éléments
// B.2.1. innerHTML
document.querySelector('.logo').innerHTML += "<small>les pizzas c'est la vie";
// B.2.2. getAttribute/setAttribute
// document.querySelector('.pizzaListLink').classList.add('active');

// C.2. Navigation en JS : afficher/masquer un élément
const newsContainer = document.querySelector('.newsContainer'),
	closeButton = newsContainer.querySelector('.closeButton');
// affichage du bandeau de news
//newsContainer.style.display = '';
// gestion du bouton fermer
/*closeButton.addEventListener('click', event => {
	event.preventDefault();
	newsContainer.style.display = 'none';
});*/

// C.3. Navigation en JS : Le menu

const pizzaList = new PizzaList([]),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];
Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.menuElement = document.querySelector('.mainMenu');

// E.2. History API
window.onpopstate = () => {
	Router.navigate(document.location.pathname, false);
};
// E.3. Le deep linking
Router.navigate(document.location.pathname);

function displayNews(html) {
	document.querySelector('.newsContainer').innerHTML += html;
	newsContainer.style.display = '';
}

fetch('./news.html')
	.then(response => response.text())
	.then(displayNews);
